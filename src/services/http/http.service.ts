import {ParsedUrlQueryInput, stringify} from 'querystring';
import {HttpHeader, HttpMethod} from './../../common/enums/enums';

interface IHeaders {
    contentType: string;
}

interface IOptions {
    method?: string;
    payload?: BodyInit;
    contentType?: string;
    query?: ParsedUrlQueryInput;
}

class Http {

    load(url: string, options: IOptions = {}) {
        const {
            method = HttpMethod.GET,
            payload = null,
            contentType,
            query
        } = options;

        const headers = this._getHeaders({
            contentType
        } as IHeaders);

        return fetch(this._getUrl(url, query), {
            method,
            headers,
            body: payload
        })
            .then(this._checkStatus)
            .then(this._parseJSON)
            .catch(this._throwError);
    }

    _getHeaders(h: IHeaders) {
        const headers = new Headers();

        if (h.contentType) {
            headers.append(HttpHeader.CONTENT_TYPE, h.contentType);
        }

        return headers;
    }

    async _checkStatus(response: any) {
        if (!response.ok) {
            const parsedException = await response.json();

            throw new Error(parsedException?.message ?? response.statusText);
        }

        return response;
    }

    _getUrl(url: string, query: ParsedUrlQueryInput | undefined) {
        return `${url}${query ? `?${stringify(query)}` : ''}`;
    }

    _parseJSON(response: any) {
        return response.json();
    }

    _throwError(err: Error) {
        throw err;
    }
}

export {Http};
