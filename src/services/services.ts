import {Http} from './http/http.service';
import {MessageService} from './message/message.service';

const http = new Http();

const messageService = new MessageService(http)

export {messageService}
