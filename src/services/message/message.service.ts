import {Http} from '../http/http.service';
import {HttpMethod} from '../../common/enums/http/HttpMethod.enum';

class MessageService {
    private readonly _http: Http;

    constructor(http: Http) {
        this._http = http;
    }

    getMessages() {
        return this._http.load('https://edikdolynskyi.github.io/react_sources/messages.json', {
            method: HttpMethod.GET
        })
    }
}

export {MessageService};
