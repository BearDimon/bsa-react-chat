import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'semantic-ui-css/semantic.min.css'
import Chat from './components/Chat/Chat';

ReactDOM.render(
    <Chat/>,
    document.getElementById('root')
);
