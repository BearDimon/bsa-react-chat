import {IMessage} from "../../common/interfaces/Chat.interfaces";
import {dateToTextFormat} from "../date/date.helper";

const formMessageMap = (messages: Readonly<IMessage[]>) => {
    let map = new Map<string, IMessage[]>();
    for (const msg of messages) {
        const date = dateToTextFormat(msg.createdAt);
        if (map.has(date))
            map.get(date)?.push(msg)
        else {
            map.set(date, [msg])
        }
    }
    map.forEach(day => day.sort((a, b) => {
        if (a.createdAt > b.createdAt) return 1;
        else if (a.createdAt < b.createdAt) return -1;
        else return 0;
    }));
    return map;
}

const formHeaderInfo = (map: Readonly<Map<string, IMessage[]>>) => {
    const msgs = Array.from(map, (([, value]) => value)).flat();
    return {
        chatName: 'React-chat',
        messageCount: msgs.length,
        userCount: new Set(msgs.map(msg => msg.userId)).size,
        lastMessageDate: msgs.map(msg => msg.createdAt)
            .sort()
            .pop()?.toLocaleString()
            .replaceAll('/', '.')
            .replace(',', ' ')
            .slice(0, -3) as string
    }
}

const findMessageInMap = (map: Readonly<Map<string, IMessage[]>>, createdAt: Date, msgId: string) => {
    const key = dateToTextFormat(createdAt);
    if (map.has(key)) {
        const msgs = map.get(key) as IMessage[];
        const index = msgs.findIndex(msg => msg.id === msgId);
        if (index !== -1) {
            return msgs[index];
        }
    }
    return null;
}

export {findMessageInMap, formHeaderInfo, formMessageMap};
