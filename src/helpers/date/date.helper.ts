import {DateToNow} from '../../common/enums/date/DateToNow.enum';
import {IMessage} from '../../common/interfaces/Chat.interfaces';

const dateToTextFormat = (date: Date) => {
    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1)
    switch (date.toDateString()) {
        case new Date().toDateString():
            return DateToNow.TODAY;
        case yesterday.toDateString():
            return DateToNow.YESTERDAY;
        default:
            return date.toLocaleDateString('en-GB', {weekday: 'long', day: 'numeric', month: 'long'});
    }
}

const dateToTime = (msg: IMessage) => {
    return {
        createdAt: msg.createdAt.toLocaleTimeString('en-GB', {timeStyle: 'short'}),
        editedAt: !msg.editedAt ? '' : msg.editedAt.toLocaleTimeString('en-GB', {timeStyle: 'short'})
    }
}

export {dateToTime, dateToTextFormat}
