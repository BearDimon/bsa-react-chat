export interface IHeaderInfo {
    chatName: string;
    userCount: number;
    messageCount: number;
    lastMessageDate: string;
}
