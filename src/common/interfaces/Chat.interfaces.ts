import {IHeaderInfo} from "./Header.interfaces";

export interface BasicMessage {
    id: string;
    userId: string;
    avatar?: string;
    user?: string;
    text: string;
    isLiked?: boolean
}

export interface StringDate {
    createdAt: string;
    editedAt: string;
}

export interface IJsonMessage extends StringDate, BasicMessage {
}

export interface IMessage extends BasicMessage {
    createdAt: Date;
    editedAt: Date | null;
}

export interface IUser {
    userId: string,
}

export interface IEditInfo {
    id: string,
    createdAt: Date,
    text: string
    isLiked?: boolean
}

export interface IChatInfo {
    messages: Map<string, IMessage[]>;
    headerInfo: IHeaderInfo;
    user: IUser
    editInfo?: IEditInfo
}
