import React from 'react';
import {IMessage} from '../../common/interfaces/Chat.interfaces';
import Message from '../Message/Message';
import './MessageList.css'
import OwnMessage from '../OwnMessage/OwnMessage';

interface MessageListProps {
    messages: Map<string, IMessage[]>;
    userId: string;
    onEdit: (msg: IMessage) => (void);
    onDelete: (msg: IMessage) => (void);
    onHandleReact: (msg: IMessage) => (void);
}

class MessageList extends React.Component<Readonly<MessageListProps>, any> {
    constructor(props: Readonly<MessageListProps>) {
        super(props);
        this.getDividedMessages.bind(this);
    }

    getDividedMessages() {
        let result = [];
        for (const key of Array.from(this.props.messages.keys())) {
            result.push(
                <h4 key={key} className='messages-divider'>{key}</h4>);
            for (const msg of this.props.messages.get(key) as IMessage[]) {
                result.push(
                    this.props.userId === msg.userId
                        ?
                        <OwnMessage key={msg.id} message={msg} onDelete={this.props.onDelete}
                                    onEdit={this.props.onEdit}/>
                        :
                        <Message key={msg.id} message={msg} onReact={this.props.onHandleReact}/>);
            }
        }
        return result;
    }

    render() {
        return (<div className='message-list'>
            {this.getDividedMessages()}
        </div>);
    }

}

export default MessageList;
