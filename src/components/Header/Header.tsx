import React from 'react';
import './Header.css';
import {IHeaderInfo} from '../../common/interfaces/Header.interfaces';

interface Props {
    headerInfo: IHeaderInfo
}

class Header extends React.Component<Props, any> {
    render() {
        return (<div className='header'>
            <h3 className='header-title'>{this.props.headerInfo.chatName}</h3>
            <h4 className='header-users-count'>{this.props.headerInfo.userCount} participants</h4>
            <h4 className='header-messages-count'>{this.props.headerInfo.messageCount} messages</h4>
            <h4 className='header-last-message-date'>last message at {this.props.headerInfo.lastMessageDate}</h4>
        </div>);
    }
}

export default Header;
