import React from 'react';
import {IMessage, StringDate} from '../../common/interfaces/Chat.interfaces';
import {Comment, Icon} from "semantic-ui-react";
import './Message.css'

interface MessageProps {
    message: IMessage
    onReact: (message: IMessage) => (void)
}

class Message extends React.Component<Readonly<MessageProps>, StringDate> {
    constructor(props: Readonly<MessageProps>) {
        super(props);
        const msg = this.props.message;
        this.state = {
            createdAt: msg.createdAt.toLocaleTimeString('en-GB', {timeStyle: 'short'}),
            editedAt: !msg.editedAt ? '' : msg.editedAt.toLocaleTimeString('en-GB', {timeStyle: 'short'})
        }
    }

    handleReaction(reaction: boolean) {
        const msg = this.props.message;
        msg.isLiked = reaction;
        this.props.onReact(msg);
    }

    render() {
        return (
            <Comment.Group className='message'>
                <Comment>
                    <Comment.Avatar className='message-user-avatar' src={this.props.message.avatar}/>
                    <Comment.Content>
                        <Comment.Author className='message-user-name' as='a'>{this.props.message.user}</Comment.Author>
                        <Comment.Metadata>
                            <div
                                className='message-time'>{this.state.createdAt} {this.state.editedAt ? 'edited' : null}</div>
                        </Comment.Metadata>
                        <Comment.Text className='message-text'>{this.props.message.text}</Comment.Text>
                        <Comment.Actions>
                            <Comment.Action>
                                {this.props.message.isLiked
                                    ?
                                    <Icon className='message-liked' name='thumbs up outline'
                                          onClick={() => this.handleReaction(false)}>Liked</Icon>
                                    :
                                    <Icon className='message-like' name='thumbs up'
                                          onClick={() => this.handleReaction(true)}>Like</Icon>
                                }
                            </Comment.Action>
                        </Comment.Actions>
                    </Comment.Content>
                </Comment>
            </Comment.Group>
        );
    }
}

export default Message;
