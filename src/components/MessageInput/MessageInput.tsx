import React from 'react';
import './MessageInput.css'
import {Form} from 'semantic-ui-react';
import {IEditInfo} from '../../common/interfaces/Chat.interfaces';

export interface MessageInputProps {
    send: (text: string) => (void);
    edit: (editInfo: IEditInfo) => (void);
    editInfo?: IEditInfo
}

interface State {
    text: string;
    isEditMode: boolean
}

class MessageInput extends React.Component<Readonly<MessageInputProps>, State> {

    constructor(props: Readonly<MessageInputProps>) {
        super(props);
        const editInfo = this.props.editInfo;
        this.state = {text: editInfo ? editInfo.text : '', isEditMode: !!editInfo};
        this.handleEdit = this.handleEdit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSend = this.handleSend.bind(this);
    }

    shouldComponentUpdate(nextProps: Readonly<Readonly<MessageInputProps>>, nextState: Readonly<State>, nextContext: any): boolean {
        if (nextProps !== this.props) {
            const editInfo = nextProps.editInfo;
            this.setState({...this.state, text: editInfo ? editInfo.text : '', isEditMode: !!editInfo});
            return true
        } else if (nextState !== this.state) return true;
        return false;
    }

    handleChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({text: e.target.value})
    }

    handleSend() {
        this.props.send(this.state.text);
        this.setState({text: ''})
    }

    handleEdit() {
        this.props.edit({...this.props.editInfo, text: this.state.text} as IEditInfo)
    }

    render() {
        return (
            <Form className='message-input'>
                <Form.Group>
                    <Form.Input width='9' onChange={this.handleChange} value={this.state.text} name='text'
                                className='message-input-text'/>
                    <Form.Button
                        width='3'
                        className='message-input-button'
                        content={this.state.isEditMode ? 'Edit' : 'Send'}
                        labelPosition='left'
                        icon='send'
                        secondary
                        onClick={() => this.state.isEditMode ? this.handleEdit() : this.handleSend()}
                    />
                </Form.Group>
            </Form>
        );
    }

}

export default MessageInput;
