import React from 'react';
import {Loader} from 'semantic-ui-react';
import './Preloader.css';

class Preloader extends React.Component {

    render() {
        return (<div className='preloader'>
            <Loader active inline='centered'/>
        </div>);
    }
}

export default Preloader;
