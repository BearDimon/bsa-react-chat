import React from 'react';
import {IMessage, StringDate} from '../../common/interfaces/Chat.interfaces';
import {Comment, Icon} from 'semantic-ui-react';
import './OwnMessage.css'
import {dateToTime} from "../../helpers/date/date.helper";

interface OwnMessageProps {
    message: IMessage;
    onEdit: (msg: IMessage) => (void);
    onDelete: (msg: IMessage) => (void);
}

class OwnMessage extends React.Component<Readonly<OwnMessageProps>, StringDate> {

    constructor(props: Readonly<OwnMessageProps>) {
        super(props);
        this.state = dateToTime(this.props.message);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    shouldComponentUpdate(nextProps: Readonly<Readonly<OwnMessageProps>>, nextState: Readonly<StringDate>, nextContext: any): boolean {
        if (nextProps !== this.props) {
            this.setState(dateToTime(nextProps.message));
            return true;
        } else if (nextState !== this.state) return true;
        return false;
    }

    handleEdit() {
        this.props.onEdit(this.props.message);
    }

    handleDelete() {
        this.props.onDelete(this.props.message);
    }


    render() {
        return (
            <Comment.Group className='own-message message'>
                <Comment>
                    <Comment.Content>
                        <Comment.Author as='a'>Me</Comment.Author>
                        <Comment.Metadata>
                            <div
                                className='message-text'>{this.state.createdAt} {this.state.editedAt ? 'edited' : null}</div>
                        </Comment.Metadata>
                        <Comment.Text className='message-time'>{this.props.message.text}</Comment.Text>
                        <Comment.Actions>
                            <Comment.Action onClick={this.handleEdit}><Icon className='message-edit'
                                                                            name='edit'/></Comment.Action>
                            <Comment.Action onClick={this.handleDelete}><Icon className='message-delete' name='delete'/></Comment.Action>
                        </Comment.Actions>
                    </Comment.Content>
                </Comment>
            </Comment.Group>
        );
    }

}

export default OwnMessage;
