import React from 'react';
import Header from '../Header/Header';
import Preloader from '../Preloader/Preloader';
import {messageService} from '../../services/services';
import {IChatInfo, IEditInfo, IJsonMessage, IMessage} from '../../common/interfaces/Chat.interfaces';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import {dateToTextFormat} from '../../helpers/date/date.helper';
import {findMessageInMap, formHeaderInfo, formMessageMap} from "../../helpers/message/message.helper";

import './Chat.css';

const {v4: uuidv4} = require('uuid');

class Chat extends React.Component<any, IChatInfo> {
    constructor(props: Readonly<any>) {
        super(props);
        this.transformResult = this.transformResult.bind(this);

        messageService.getMessages().then(this.transformResult);

        this.handleSend = this.handleSend.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleChangeMessage = this.handleChangeMessage.bind(this);
    }

    private transformResult(result: any) {
        let r = result as IJsonMessage[];
        let msgs = [];
        for (const msg of r) {
            msgs.push({
                ...msg,
                createdAt: new Date(msg.createdAt),
                editedAt: msg.editedAt ? new Date(msg.editedAt) : null
            })
        }

        const map = formMessageMap(msgs);

        this.setState(
            {
                user: {
                    userId: uuidv4()
                },
                messages: map,
                headerInfo: formHeaderInfo(map)
            }
        );
    }

    handleSend(text: string) {
        const msg: IMessage = {
            id: uuidv4(),
            userId: this.state.user.userId,
            createdAt: new Date(),
            editedAt: null,
            text: text
        }
        const date = dateToTextFormat(new Date());
        const map = this.state.messages;
        if (map.has(date))
            map.get(date)?.push(msg)
        else {
            map.set(date, [msg])
        }
        this.setState({...this.state, headerInfo: formHeaderInfo(map)});
    }

    handleEdit(editMsg: IMessage) {
        this.setState({
            ...this.state, editInfo: {
                id: editMsg.id,
                createdAt: editMsg.createdAt,
                text: editMsg.text
            }
        });
    }

    handleDelete(deleteMsg: IMessage) {
        const key = dateToTextFormat(deleteMsg.createdAt);
        const map = this.state.messages;
        if (map.has(key)) {
            const msgs = map.get(key) as IMessage[];
            const filteredMsgs = msgs.filter(msg => msg.id !== deleteMsg.id);
            if (filteredMsgs.length === 0)
                map.delete(key);
            else
                map.set(key, filteredMsgs);
            this.setState({...this.state, headerInfo: formHeaderInfo(map)});
        }
    }

    handleChangeMessage(data : IEditInfo) {
        const map = this.state.messages;
        const msg = findMessageInMap(map, data.createdAt, data.id);
        if (msg) {
            msg.text = data.text;
            msg.isLiked = data.isLiked ? data.isLiked : false
            msg.editedAt = new Date();
            this.setState({...this.state, editInfo: undefined});
        }
    }

    render() {
        return (<div>
            {!this.state
                ?
                <Preloader/>
                :
                <>
                    <div className='main-logo'>
                        <svg width="70" height="70" viewBox="0 0 105 99">
                            <g fill="none">
                                <path
                                    d="M59.927 76.881c.267.616-.427 1.205-.99.838L29.31 58.376c-2.259-1.299-1.743-3.772 0-4.526l16.585-6.125c.65-.24 1.37.07 1.647.703l12.386 28.453z"
                                    fill="#FFC712"/>
                                <path
                                    d="M87.193 73.214L68.999 81.09c-1.16.503-2.546-.125-3.1-1.402L42.465 25.565c-.555-1.28-.061-2.726 1.101-3.23l18.193-7.876c1.16-.502 2.548.125 3.101 1.402l23.436 54.124c.554 1.28.06 2.726-1.102 3.23"
                                    fill="#00ADEE"/>
                                <path
                                    d="M39.682 30.034c1.279 2.9 4.213 9.704 5.36 12.368a1.137 1.137 0 01-.646 1.516l-12.679 4.757a.678.678 0 01-.859-.904l7.633-17.73a.65.65 0 011.191-.007M40.351 70.35l-13.924-9.294a1.066 1.066 0 00-1.567.46l-7.976 18.206c-.404.924.53 1.864 1.455 1.465l21.889-9.473a.78.78 0 00.123-1.363"
                                    fill="#EC1848"/>
                                <path
                                    d="M40.351 70.35l-13.924-9.294a1.066 1.066 0 00-1.567.46l9.402 12.78 5.966 -2.582a.78.78 0 00.123-1.363"
                                    fill="#C22035"/>
                            </g>
                        </svg>
                        <h2>BSA-REACT</h2>
                    </div>
                    <div className='main-wrapper'>
                        <Header
                            headerInfo={this.state.headerInfo}
                        />
                        <MessageList messages={this.state.messages} userId={this.state.user.userId}
                                     onEdit={this.handleEdit} onDelete={this.handleDelete}
                                     onHandleReact={this.handleChangeMessage}
                        />
                        <MessageInput send={this.handleSend} edit={this.handleChangeMessage}
                                      editInfo={this.state.editInfo}/>
                    </div>
                </>
            }
        </div>);
    }
}

export default Chat;
